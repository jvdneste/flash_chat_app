import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCupertinoAlertDialog extends StatelessWidget {
  CustomCupertinoAlertDialog({
    this.titleText,
    this.contentText,
    this.textButtonNo,
  });

  final titleText;
  final contentText;
  final textButtonNo;
  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(
        '$titleText',
        style: TextStyle(color: Colors.red),
      ),
      content: Text('$contentText'),
      actions: <Widget>[
        textButtonNo != null
            ? CupertinoDialogAction(
                child: Text('$textButtonNo'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            : Container(),
      ],
    );
  }
}
