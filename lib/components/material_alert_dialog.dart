import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {
  CustomAlertDialog({
    this.titleText,
    this.contentText,
    this.textButtonNo,
  });

  final titleText;
  final contentText;
  final textButtonNo;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        '$titleText',
        style: TextStyle(color: Colors.red),
      ),
      content: Text('$contentText'),
      actions: <Widget>[
        textButtonNo != null
            ? FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('$textButtonNo'))
            : Container(),
      ],
    );
  }
}
