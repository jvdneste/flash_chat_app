import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';

class MessageBubble extends StatelessWidget {
  MessageBubble({this.text, this.imageURL, this.sender, this.isMe, this.time});

  final String sender;
  final String text;
  final bool isMe;
  final String time;
  final String imageURL;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: text != null || imageURL != null
          ? Column(
              crossAxisAlignment:
                  isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  sender,
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Colors.black54,
                  ),
                ),
                Material(
                  borderRadius: isMe
                      ? BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          bottomLeft: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0))
                      : BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomLeft: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0)),
                  elevation: 5.0,
                  color: isMe ? Colors.lightBlueAccent : Colors.white,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: 15.0,
                    ),
                    child: Column(
                      children: <Widget>[
                        imageURL != null
                            ? TransitionToImage(
                                image: AdvancedNetworkImage(
                                  imageURL,
                                  loadedCallback: () => print('It works!'),
                                  loadFailedCallback: () => print('Oh, no!'),
                                  timeoutDuration: Duration(seconds: 30),
                                  retryLimit: 1,
                                ),
                                fit: BoxFit.contain,
                                placeholder: Container(
                                  width: 300.0,
                                  height: 300.0,
                                  color: Colors.transparent,
                                  child: const Icon(Icons.refresh),
                                ),
                                imageFilter: ImageFilter.blur(
                                    sigmaX: 10.0, sigmaY: 10.0),
                                width: 300.0,
                                height: 300.0,
                                enableRefresh: true,
                                loadingWidgetBuilder: (
                                  BuildContext context,
                                  double progress,
                                  Uint8List imageData,
                                ) {
                                  // print(imageData.lengthInBytes);
                                  return Container(
                                    width: 300.0,
                                    height: 300.0,
                                    alignment: Alignment.center,
                                    child: CircularProgressIndicator(
                                      value: progress == 0.0 ? null : progress,
                                    ),
                                  );
                                },
                              )
                            : Container(width: 0, height: 0),
                        text != null
                            ? Text(
                                text,
                                style: TextStyle(
                                  color: isMe ? Colors.white : Colors.black54,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              )
                            : Container(width: 0, height: 0),
                      ],
                    ),
                  ),
                ),
              ],
            )
          : Text(
              '<Error 404 while downloading image>',
              style: TextStyle(fontSize: 8),
              textAlign: isMe ? TextAlign.right : TextAlign.left,
            ),
    );
  }
}
