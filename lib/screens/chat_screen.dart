import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flash_chat/components/cupertino_alert_dialog.dart';
import 'package:flash_chat/components/material_alert_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;

import '../constants.dart';
import '../streams/messages_stream.dart';

final firestore = Firestore.instance;
FirebaseUser loggedInUser;

class ChatScreen extends StatefulWidget {
  static const String id = 'chat_screen';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageTextController = TextEditingController();
  final _auth = FirebaseAuth.instance;

  String messageText;

  @override
  void initState() {
    super.initState();

    getCurrentUser();
  }

  void getCurrentUser() async {
    try {
      final user = await _auth.currentUser();
      if (user != null) {
        loggedInUser = user;
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                _auth.signOut();
                Navigator.pop(context);
              }),
        ],
        title: Text('⚡️Chat'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      //
      //
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            MessagesStream(),
            Container(
              decoration: kMessageContainerDecoration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
                        uploadFile(image);
                      });
                    },
                    child: Icon(
                      Icons.image,
                      size: 35,
                      color: Colors.lightBlueAccent,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: messageTextController,
                      onChanged: (value) {
                        messageText = value;
                      },
                      decoration: kMessageTextFieldDecoration,
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      messageText != null
                          ? firestore.collection('messages').add({
                              'text': messageText,
                              'sender': loggedInUser.email,
                              'timestamp': FieldValue.serverTimestamp(),
                            })
                          : Platform.isIOS
                              ? showCupertinoDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      CustomCupertinoAlertDialog(
                                        titleText: 'Warning',
                                        contentText:
                                            'Your message haven`t anything!',
                                        textButtonNo: 'Dismiss',
                                      ))
                              : showDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      CustomAlertDialog(
                                        titleText: 'Warning',
                                        contentText:
                                            'Your message haven`t anything!',
                                        textButtonNo: 'I use Android',
                                      ));

                      messageTextController.clear();
                      messageText = null;
                    },
                    child: Text(
                      'Send',
                      style: kSendButtonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<File> chooseImage() async {
    return ImagePicker.pickImage(source: ImageSource.gallery);
  }

  void uploadFile(final File image) {
    final String id = UniqueKey().toString();
    final StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('chats/$id');

    storageReference.putFile(image).onComplete.then((snapshot) {
      print('File Uploaded');
      storageReference.getDownloadURL().then((fileURL) {
        firestore.collection('messages').add({
          'imageurl': fileURL,
          'sender': loggedInUser.email,
          'timestamp': FieldValue.serverTimestamp(),
        });
      });
    });
  }
}
